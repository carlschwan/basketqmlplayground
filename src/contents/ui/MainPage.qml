/**
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.12
import QtQml.Models 2.1
import QtQuick.Layouts 1.14
import QtQuick.Controls 2.14 as QQC2
import org.kde.kirigami 2.12 as Kirigami

Kirigami.Page {
    title: "Basket 1"
    
    property alias isDrawing: drawButton.checked;
    
    header: QQC2.ToolBar {
        RowLayout {
            anchors.fill: parent
            QQC2.ToolButton {
                id: drawButton
                icon.name: "draw-freehand"
                checkable: true
            }
        }
    }
    
    ListModel {
        id: colorModel
        ListElement { x: 20; y: 20; text: "My Note 1" }
        ListElement { x: 90; y: 140; text: "My Note 2" }
        ListElement { x: 140; y: 90; text: "My Note 3" }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: colorModel.append({ color: "yellow", x: mouseX, y: mouseY })
    }

    Repeater {
        model: colorModel
        delegate: Note { 
            noteModel: model 
        }
    }
}
