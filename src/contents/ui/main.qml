/**
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.3
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.2 as Controls
import org.kde.kirigami 2.1 as Kirigami

Kirigami.ApplicationWindow {
    id: root
    width: 10000
    height: 800
    visible: true

    signal startChat(string personUri)

    globalDrawer: Kirigami.GlobalDrawer {
        title: "Basket"
        titleIcon: "org.kde.basket"
        modal: false
        collapsible: true
        collapsed: false

        actions: [
            Kirigami.Action {
                text: "Basket 1"
                iconName: ""
            }
        ]
    }
    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: mainPageComponent

    //Main app content
    Component {
        id: mainPageComponent

        MainPage {}
    }
}
