/**
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
import QtQuick 2.12
import QtQuick.Layouts 1.14
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.14 as QQC2
import org.kde.kirigami 2.12 as Kirigami
import org.kde.plasma.core 2.0 as PlasmaCore

import org.kde.basket.texteditor 1.0

PlasmaCore.SvgItem {
    id: root

    svg: PlasmaCore.Svg {
        imagePath: "widgets/notes"
    }
    property var noteModel: {};
    x: noteModel.x; y: noteModel.y
    width: 250; height: 200
    
    Drag.active: dragArea.drag.active

    ColorDialog {
        id: colorDialog
        currentColor: "black"
    }

    NoteHandler {
        id: document
        document: textArea.textDocument
        cursorPosition: textArea.cursorPosition
        selectionStart: textArea.selectionStart
        selectionEnd: textArea.selectionEnd
        textColor: colorDialog.color

        onLoaded: {
            console.log(text)
            textArea.text = text
        }

        onError: {
            errorDialog.text = message
            errorDialog.visible = true
        }
    }
    
    RowLayout {
        anchors.fill: parent

        MouseArea {
            Layout.minimumWidth: 20
            Layout.fillHeight: true
            id: dragArea
            cursorShape: Qt.DragMoveCursor

            drag.target: root
        }

        Flickable {
            id: flickable
            flickableDirection: Flickable.VerticalFlick
            Layout.fillHeight: true
            Layout.fillWidth: true

            QQC2.TextArea.flickable: QQC2.TextArea {
                id: textArea
                textFormat: Qt.RichText
                wrapMode: QQC2.TextArea.Wrap
                readOnly: false
                persistentSelection: true
                // Different styles have different padding and background
                // decorations, but since this editor is almost taking up the
                // entire window, we don't need them.
                leftPadding: 0
                rightPadding: 0
                topPadding: 6
                bottomPadding: 6
                background: null
                onLinkActivated: Qt.openUrlExternally(link)
            }

            QQC2.ScrollBar.vertical: QQC2.ScrollBar {}
        }
    }
    Rectangle {
        anchors.horizontalCenter: parent.right
        anchors.verticalCenter: parent.verticalCenter
        width: 10
        height: 10
        radius: 10
        color: "steelblue"
        MouseArea {
            anchors.fill: parent
            drag{ target: parent; axis: Drag.XAxis }
            cursorShape: Qt.SizeHorCursor
            onMouseXChanged: {
                if (drag.active) {
                    console.log(root.width - mouseX, root.width, mouseX)
                    root.width = root.width + mouseX
                    if (root.width < 50) {
                        root.width = 50
                    }
                }
            }
        } 
    }
}
