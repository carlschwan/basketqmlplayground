# SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-License-Identifier: GPL-2.0-or-later
set(basket_SRCS
    main.cpp
    notehandler.cpp
    )

qt5_add_resources(RESOURCES resources.qrc)
add_executable(basket ${basket_SRCS} ${RESOURCES})
target_link_libraries(basket Qt5::Core Qt5::Qml Qt5::Quick Qt5::Svg KF5::DBusAddons KF5::I18n)
install(TARGETS basket ${KF5_INSTALL_TARGETS_DEFAULT_ARGS})
